# TAZ v.1.0 Marlin firmware

Last edited by Ubuntourist (ubuntourist@hacdc.org) (kjc) 2019.02.14

See the [HacDC wiki Lulzbot TAZ 1.0
entry](https://wiki.hacdc.org/index.php?title=Lulzbot_TAZ_1.0)

Determining which version of the TAZ and connecting to it
---------------------------------------------------------

The "newest" addition to our collection of 3D printers is actually the
oldest: The Lulzbot TAZ is actually, a Lulzbot TAZ 1.0 (according to the
serial number on the back: KT-PR0010-5147).

The `index.html` at <http://devel.lulzbot.com/TAZ/> says, among other
things: `*"B" - KT-PR0010 - TAZ 1.0`

Using `minicom`, the baud rate was determined to be **115200**. Sending
`G28` to home it made it grind gears.

    $ pronsole
    offline> connect /dev/ttyACM2 115200
    ttyACM2 21°>

Eventually, I found
<http://download.lulzbot.com/TAZ/1.0/hardware/electronics/RAMBo/> which
identifies the board as "based off of the **Arduino MEGA**" with "5
stepper drivers" and "5 MOSFET switched outputs" with documentation
at <http://reprap.org/wiki/rambo>

As near as I can tell, the latest in firmware and other software lives
at
<http://download.lulzbot.com/TAZ/1.0/software/current/firmware/Marlin/>

Lulzbot offers instructions on [re-flashing your 3D printer's
firmware](https://www.lulzbot.com/content/re-flashing-your-3d-printers-firmware)
and see also RepRap's [RAMBo
firmware](https://www.reprap.org/wiki/Rambo_firmware) page.

Because we neither have the original paperwork for the TAZ, nor were the
EEPROM settings previously enabled, we may be forced to determine the
ESTEPS settings via, IMHO, a method fraught with inaccuracy by trying to
[manually calibrate using the OHAI
instructions](https://ohai.lulzbot.com/project/extruder_calibration/calibration/).

Adding a dynamic symlink to the TAZ...
--------------------------------------

After massaging the OS with the procedures listed in [Dynamic symlinks
for 3D printers](Dynamic_symlinks_for_3D_printers "wikilink") we see:

    $ pronsole
    WARNING:root:Could not setup DBus for sleep inhibition: org.freedesktop.DBus.Error.NotSupported: Unable to autolaunch a dbus-daemon without a $DISPLAY for X11
    Welcome to the printer console! Type "help" for a list of available commands.

    offline>
    offline> connect /dev/TAZ 115200
    start
    Printer is now online
    External Reset
    Marlin 1.0.0
    Last Updated: 2013-03-06 | Author: LulzBot
    Compiled: Apr 17 2015
    Free Memory: 5504  PlannerBufferBytes: 1232
    TAZ PC>
    TAZ 21°>

Note the details on the firmware. Namely:

-   Marlin **1.0.0**
-   Last Updated: **2013-03-06** | Author: **LulzBot**
-   Compiled: **Apr 17 2015**

Fixing Arduino
--------------

The Arduino IDE complains

    Ignoring bad library name <@pronterhost>

    The library "U8glib-new" cannot be used.

    Library names must contain only basic letters and numbers.

    (ASCII only and no spaces, and it cannot start with a number.)

Und zo, after tracking down the symbolic links and such...

    sudo -i
    $ cd /usr/share/arduino/libraries
    $ rm U8glib-new
    $ mv /home/demo/Taz/Marlin/ArduinoAddons/Arduino_1.x.x/libraries/U8glib-new /home/demo/Taz/Marlin/ArduinoAddons/Arduino_1.x.x/libraries/U8glibNew
    $ ln -s /home/demo/Taz/Marlin/ArduinoAddons/Arduino_1.x.x/libraries/U8glibNew .
    $ exit

Calibrating the TAZ
-------------------

    $ pronsole
    WARNING:root:Could not setup DBus for sleep inhibition: org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.ScreenSaver was not provided by any .service files
    Welcome to the printer console! Type "help" for a list of available commands.
    offline> connect /dev/TAZ 115200
    Printer is now online
    TAZ 21°> move z 10
    TAZ 21°> move x 10
    TAZ 21°> move x 10
    TAZ 21°> move x 10
    TAZ 21°> move x 10
    TAZ 21°> move x 10
    TAZ 21°> move x 10
    TAZ 21°> move x 10
    TAZ 21°> settemp 215
    Setting hotend temperature to 215.0 degrees Celsius.
    TAZ 21°>
    TAZ 21°/215°>
    TAZ 21°/215°>
    TAZ 21°/215°>
    TAZ 22°/215°>
       .
       .
       .
    TAZ 215°/215°>
    TAZ 215°/215°> extrude 10
    TAZ 215°/215°> extrude 10
    TAZ 215°/215°> extrude 10
    TAZ 215°/215°> extrude 40
       .
       .
       .

**NOTE:** Moving Z 10 "mm" resulted in the extruder assembly climbing
WAY too high (especially compared to the multiple X movements which,
even after 7 of them resulted in considerably less distance total.)

(Extrusion repeated until green filament residue became clear filament.)
At which point Bobby took over and deemed the current feed calibration
to be "close enough" but the ESTEPS value is still unknown.

BTW, Bobby says the **Y** calibration is **0.24 mm per step**.

Fun with Firmware
-----------------

Downloading all the firmware:

    $ wget -r -np -R "index.html*" -e robots=off http://download.lulzbot.com/TAZ/1.0/software/current/firmware/Marlin/Marlin/

In order to get the current settings from the TAZ, `EEPROM_SETTINGS`
needs to be _enabled_ which is apparently done in `Configuration.h`

Hopefully, the arcane details can be ferreted out of the [Marlin
Configuration,h
documentation](http://marlinfw.org/docs/configuration/configuration.html)

First, `Configuration.h` has ridiculously long lines, now word-wrapped
for sanity.

Next, a more vexing problem, I think: `pronterhost` has `Arduino
v.2.1.0.5+dfsg2-4.1`. _Naturally_, Marlin only offers Arduino add-ons
for `Arduino 0.xx` and `Arduino 1.x.x`.

Fortunately, further research yielded [a tidbit from the Arduino
Playground](https://playground.arduino.cc/Code/Spi): _An SPI library
is now included in the Arduino software. See the SPI library reference
for details._

    $ cd Marlin/ArduinoAddons
    $ find . -type f
    
    ./Arduino_0.xx/Gen6/boards.txt
    ./Arduino_0.xx/libraries/SPI/keywords.txt
    ./Arduino_0.xx/libraries/SPI/SPI.cpp
    ./Arduino_0.xx/libraries/SPI/SPI.h
    ./Arduino_0.xx/libraries/LiquidCrystal/keywords.txt
    ./Arduino_0.xx/libraries/LiquidCrystal/LiquidCrystal.cpp
    ./Arduino_0.xx/libraries/LiquidCrystal/LiquidCrystal.h
    ./Arduino_0.xx/Sanguino/boards.txt
    ./Arduino_0.xx/Sanguino/programmers.txt
    ./Arduino_0.xx/Gen7/boards.txt
    
    ./Arduino_1.x.x/libraries/SPI/keywords.txt
    ./Arduino_1.x.x/libraries/SPI/SPI.cpp
    ./Arduino_1.x.x/libraries/SPI/SPI.h
    ./Arduino_1.x.x/libraries/LiquidCrystal/keywords.txt
    ./Arduino_1.x.x/libraries/LiquidCrystal/LiquidCrystal.cpp
    ./Arduino_1.x.x/libraries/LiquidCrystal/LiquidCrystal.h
    ./Arduino_1.x.x/sanguino/boards.txt

According to the [SPI library reference
documentation](https://www.arduino.cc/en/reference/SPI), the `SPI`
stands for `Serial Peripheral Interface`.

The [LiquidCrystal
library](https://www.arduino.cc/en/Reference/LiquidCrystal), as one
might guess, allows Arduino to control LCDs. Since we don't have
an LCD on this machine, it's currently irrelevant.

In a previous life, I used a tool called `dfu-programmer` to flash a
PrintrBot's firmware...  (Under Arch Linux, I have both the
[dfu-programmer](https://dfu-programmer.github.io/) and
[dfu-util](http://dfu-util.sourceforge.net/) packages installed.)

----

Backup the flash memory:
------------------------

    $ avrdude -C /etc/avrdude.conf -p m2560 -c wiring -P /dev/TAZ -b 115200 -U flash:r:flash-2019-02-14.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9801 (probably m2560)
    avrdude: reading flash memory:
    
    Reading | ################################################## | 100% 33.56s
    
    avrdude: writing output file "flash-2019-02-14.hex"
    
    avrdude: safemode: Fuses OK (E:FD, H:D0, L:FF)
    
    avrdude done.  Thank you.

----

Backup the EEPROM:
------------------

    $ avrdude -C /etc/avrdude.conf -p m2560 -c wiring -P /dev/TAZ -b 115200 -U eeprom:r:eeprom-2019-02-14.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9801 (probably m2560)
    avrdude: reading eeprom memory:
    
    Reading | ################################################## | 100% 4.20s
    
    avrdude: writing output file "eeprom-2019-02-14.hex"
    
    avrdude: safemode: Fuses OK (E:FD, H:D0, L:FF)
    
    avrdude done.  Thank you.

----

Examining the dumped EEPROM .hex file above seems to indicate that
it's empty: Basically filled with binary 1's (a.k.a. Hex FF, integer
-1, Boolean TRUE).

See [active-configuration.md](active-configuration.md) for a look
which lines are not commented out in Configuration.h.

For comparison, here's what the Lulzbot Mini reports (word wrapped for
sanity):

    $ pronsole
    WARNING:root:Could not setup DBus for sleep inhibition:
                 org.freedesktop.DBus.Error.ServiceUnknown: The name
                 org.freedesktop.ScreenSaver was not provided by any
                 .service files
    WARNING:root:RPC server bound on non-default port 7979
    Welcome to the printer console! Type "help" for a list of available commands.
    offline> connect /dev/MINI
    start
    Printer is now online
    Marlin 1.1.0.11
    Last Updated: 2016-04-27 12:00 | Author: (Aleph Objects Inc., LulzBot Mini)
    Compiled: Dec 21 2016
    Free Memory: 4404  PlannerBufferBytes: 1232
    V23 stored settings retrieved (396 bytes)
    Steps per unit:
    M92 X100.50 Y100.50 Z1600.00 E833.00
    Maximum feedrates (mm/s):
    M203 X800.00 Y800.00 Z8.00 E40.00
    Maximum Acceleration (mm/s2):
    M201 X9000 Y9000 Z100 E1000
    Accelerations: P=printing, R=retract and T=travel
    M204 P2000.00 R3000.00 T2000.00
    Advanced variables: S=Min feedrate (mm/s),
                        T=Min travel feedrate (mm/s),
                        B=minimum segment time (ms),
                        X=maximum XY jerk (mm/s),
                        Z=maximum Z jerk (mm/s),
                        E=maximum E jerk (mm/s)
    M205 S0.00 T0.00 B20000 X12.00 Z0.40 E10.00
    Home offset (mm):
    M206 X0.00 Y0.00 Z0.00
    PID settings:
    M301 P28.79 I1.91 D108.51 C100.00 L20
    M304 P294.00 I65.00 D382.00
    Filament settings: Disabled
    M200 D3.00
    M200 D0
    Z-Probe Offset (mm):
    M851 Z-1.43
    MINI PC>

Upgrading the Arduino IDE
-------------------------

The MatterHackers article "[How To Successfully Flash Your 3D
Printer's
Firmware](https://www.matterhackers.com/news/how-to-successfully-flash-your-3d-printers-firmware)"
indicates we need a special [RAMBo
boards](https://raw.githubusercontent.com/ultimachine/ArduinoAddons/master/package_ultimachine_index.json)
JSON file to install into the Arduino IDE...

The instructions for installing don't match what I
see... Sigh. Naturally, the Linux version has version number
suggesting it is much more recent than whatever Arduino guides suggest
using... But, wrong.  And also naturally, there's nothing distributed
as a .deb package -- at least not from a source I trust. So. I need to
install using the official [Arduino Guide for
Linux](https://www.arduino.cc/en/Guide/Linux).


    $ sudo dpkg -P arduino arduino-core
    [sudo] password for ubuntourist:
    (Reading database ... 336652 files and directories currently installed.)
    Removing arduino (2:1.0.5+dfsg2-4.1) ...
    Purging configuration files for arduino (2:1.0.5+dfsg2-4.1) ...
    Removing arduino-core (2:1.0.5+dfsg2-4.1) ...
    dpkg: warning: while removing arduino-core, directory '/usr/share/arduino/libraries/Wire/utility' not empty so not removed
    dpkg: warning: while removing arduino-core, directory '/usr/share/arduino/libraries/SPI' not empty so not removed
    dpkg: warning: while removing arduino-core, directory '/usr/share/arduino/libraries/LiquidCrystal' not empty so not removed
    dpkg: warning: while removing arduino-core, directory '/usr/share/arduino/hardware/tools' not empty so not removed
    Processing triggers for shared-mime-info (1.10-1) ...
    Processing triggers for man-db (2.8.5-2) ...
    Processing triggers for hicolor-icon-theme (0.17-2) ...
    Processing triggers for mime-support (3.62) ...
    Processing triggers for desktop-file-utils (0.23-4) ...
    $

And this is why we document... Apparently, the stock Arduino Debian
packages have been futzed with, and I apparently wiped whatever mods
might have been there.

    /usr/share/arduino/libraries/Wire/utility$ ls -Al
    total 36
    -rw-r--r-- 1 demo demo 16386 Oct 19  2013 '#twi.c#'
    lrwxrwxrwx 1 demo demo    49 Oct 19  2013  .#twi.c -> demo@pronterhost.organleggers.org.3334:1381110447
    -rw-r--r-- 1 demo demo 16366 May  5  2013  twi.c~
    ubuntourist@pronterhost:/usr/share/arduino/libraries/Wire/utility$ diff -u twi.c~ \#twi.c#
    --- twi.c~      2013-05-05 13:55:52.000000000 -0500
    +++ #twi.c#     2013-10-19 20:10:50.817962597 -0500
    @@ -202,6 +202,7 @@
       while(TWI_READY != twi_state){
         continue;
       }
    +
       twi_state = TWI_MTX;
       twi_sendStop = sendStop;
       // reset error state (0xFF.. no error occured)
    @@ -210,11 +211,12 @@
       // initialize buffer iteration vars
       twi_masterBufferIndex = 0;
       twi_masterBufferLength = length;
    -
    +
       // copy data to twi buffer
       for(i = 0; i < length; ++i){
         twi_masterBuffer[i] = data[i];
       }
    +
    
       // build sla+w, slave device address + w bit
       twi_slarw = TW_WRITE;
    @@ -224,6 +226,7 @@
       // in the ISR. Don't do it again.
       //
       if (true == twi_inRepStart) {
    +
         // if we're in the repeated start state, then we've already sent the start,
         // (@@@ we hope), and the TWI statemachine is just waiting for the address byte.
         // We need to remove ourselves from the repeated start state before we enable interrupts,
    @@ -242,7 +245,7 @@
       while(wait && (TWI_MTX == twi_state)){
         continue;
       }
    -
    +  return twi_state;
       if (twi_error == 0xFF)
         return 0;  // success
       else if (twi_error == TW_MT_SLA_NACK)
    $

And, whatever `demo@pronterhost.organleggers.org.3334:1381110447` was,
it appears to be no more. The symbolic link above points nowhere. A more
complete picture of what the purge did not remove:

    $ cd /usr/share/arduino
    $ ls -AR
    .:
    hardware  libraries
    
    ./hardware:
    Sanguino  tools
    
    ./hardware/Sanguino:
    boards.txt  bootloaders  cores  programmers.txt
    
    ./hardware/Sanguino/bootloaders:
    atmega644p
    
    ./hardware/Sanguino/bootloaders/atmega644p:
    ATmegaBOOT_324P.hex  ATmegaBOOT_644.hex  ATmegaBOOT_644P.hex  ATmegaBOOT.c  Makefile  README.txt
    
    ./hardware/Sanguino/cores:
    arduino
    
    ./hardware/Sanguino/cores/arduino:
     binary.h             main.cpp         Print.h        WConstants.h      wiring_digital.c   wiring_shift.c   WString.h
    'Copy of wiring.h'    pins_arduino.c   Stream.h       WInterrupts.c     wiring.h           WMath.cpp
     HardwareSerial.cpp   pins_arduino.h   Tone.cpp       wiring_analog.c   wiring_private.h   WProgram.h
     HardwareSerial.h     Print.cpp        WCharacter.h   wiring.c          wiring_pulse.c     WString.cpp
    
    ./hardware/tools:
    '#avrdude.conf#'
    
    ./libraries:
    hardware  libraries  LiquidCrystal  SPI  U8glib  u8glib_arduino_v1.18.1.zip  U8glibNew  Wire
    
    ./libraries/LiquidCrystal:
    LiquidCrystal
    
    ./libraries/SPI:
    SPI
    
    ./libraries/Wire:
    utility
    
    ./libraries/Wire/utility:
    '#twi.c#'   .#twi.c   twi.c~
    
    $ cd ~/
    $ mkdir build
    $ cd buid
    $ tar xJvf arduino-1.8.8-linux32.tar.xz
    $  sudo ./install.sh
    Adding desktop shortcut, menu item and file associations for Arduino IDE... done!

Gah! Who said I wanted a desktop?

    $ sudo ./uninstall.sh
    Removing desktop shortcut and menu item for Arduino IDE... done!

So. I have to run from within the user directory where I
untarred. Bleah! Let's not.

    $ sudo -i
    $ cp -vr arduino-1.8.8 /opt/
    $ mkdir /opt/bin
    $ cd /opt/bin
    $ ln -s ../arduino-1.8.8/arduino .
    $ cd /etc
    $ edit profile
    ... add /opt/bin to start of PATH ...
    PATH=/opt/bin:...
    $ exit
    $ rm -rf arduino-1.8.8

Compiling and uploading with modified Configuration.h
-----------------------------------------------------

    $ arduino
    
    ...Preferences -> Additional Boards Manager URLs
    ...https://raw.githubusercontent.com/ultimachine/ArduinoAddons/master/package_ultimachine_index.json
    ...Tools -> Board -> Board Manager -> RAMBo -> Install
    ...Tools -> Board -> Rambo
    ...Tools -> Port -> /dev/ttyACM1  # SUBJECT TO CHANGE !!!
    ...File -> Load -> Marlin.ino     # (Navigate to directory first)
    ...(checkbox, i.e. compile)
    ...(right arrow, i.e. upload)
    
    Sketch uses 48566 bytes (18%) of program storage space. Maximum is 258048 bytes.
    Global variables use 2627 bytes of dynamic memory.
    Sketch uses 48566 bytes (18%) of program storage space. Maximum is 258048 bytes.
    Global variables use 2627 bytes of dynamic memory.
    ubuntourist@pronterhost:~/build/arduino-1.8.8$ pronsole
    WARNING:root:Could not setup DBus for sleep inhibition: org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.ScreenSaver was not provided by any .service files
    WARNING:root:RPC server bound on non-default port 7979
    Welcome to the printer console! Type "help" for a list of available commands.
    offline> connect /dev/TAZ
    start
    Printer is now online
    External Reset
    Marlin 1.0.0
    Last Updated: 2013-03-06 | Author: LulzBot
    Compiled: Feb 14 2019
    Free Memory: 5429  PlannerBufferBytes: 1232
    Using Default settings:
    Steps per unit:
    M92 X100.50 Y100.50 Z800.00 E800.00
    Maximum feedrates (mm/s):
    M203 X500.00 Y500.00 Z10.00 E45.00
    Maximum Acceleration (mm/s2):
    M201 X9000 Y9000 Z100 E10000
    Acceleration: S=acceleration, T=retract acceleration
    M204 S500.00 T3000.00
    Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum xY jerk (mm/s),  Z=maximum Z jerk (mm/s)
    M205 S0.00 T0.00 B20000 X5.00 Z0.40 E5.00
    Home offset (mm):
    M206 X0.00 Y0.00 Z0.00
    PID settings:
    M301 P22.20 I1.08 D114.00
    TAZ PC>
    TAZ 22°> M501
    SENDING:M501
    Using Default settings:
    Steps per unit:
    M92 X100.50 Y100.50 Z800.00 E800.00
    Maximum feedrates (mm/s):
    M203 X500.00 Y500.00 Z10.00 E45.00
    Maximum Acceleration (mm/s2):
    M201 X9000 Y9000 Z100 E10000
    Acceleration: S=acceleration, T=retract acceleration
    M204 S500.00 T3000.00
    Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), X=maximum xY jerk (mm/s),  Z=maximum Z jerk (mm/s)
    M205 S0.00 T0.00 B20000 X5.00 Z0.40 E5.00
    Home offset (mm):
    M206 X0.00 Y0.00 Z0.00
    PID settings:
    M301 P22.20 I1.08 D114.00
    TAZ 22°>

Success!

More calibration info
---------------------

**THIS SECTION NEEDS LOTS OF WORK!!!**

This is a bit of a brain-dump of info at the moment.

Banggold 3D Brass Nut for Z axis rod

Sources:

* [Banggood brass flange nut for 3D-printer Z-axis 8mm stainless
  steel](https://www.banggood.com/Brass-Flange-Nut-For-3D-Printer-Z-Axis-8mm-Stainless-Steel-p-973107.html?cur_warehouse=CN)
* [Drillpro 400mm lead
  screw](https://www.amazon.com/Drillpro-400mm-Lead-Screw-Printer/dp/B017AR5QBS)

The rod (screw) specs:

    Lead screw diameter:      8 mm
    Lead screw Length:      400 mm
    Pitch (a.k.a. spacing):   2 mm (threads are 2 mm apart?)
    Lead:                     8 mm (360-degree turn = 8 mm displacement?)
    Lead screw (of) thread:   8 mm (360-degree turn = 8 mm displacement?)

    Therefore: 4 "starts" (* 2 mm pitch = 8 mm lead or thread)

* [Would using a leadscrew with 1 or 2 mm lead, en lieu of 8 mm,
  result in a better
  printer?](https://3dprinting.stackexchange.com/questions/4031/would-using-a-leadscrew-with-1-or-2-mm-lead-en-lieu-of-8-mm-result-in-a-better)
* YouTube: [Still Extruding: Calibrating XY&Z
  Step/MM](https://www.youtube.com/watch?v=wAL9d7FgInk)
* See "Sources of Documentation" in [Marlin Firmware
  Configuration](http://marlinfw.org/docs/configuration/configuration.html)
* [3D Printer Firmware Settings - Stepper Motor
  Configuration](https://www.matterhackers.com/news/3d-printer-firmware-settings-stepper-motor-configuration)

As near as I can tell, we have a `NEMA 17 with standard pitch M8
threaded rod` and the formula for the steps per mm, taken from
[Triffid Hunter's Calibration
Guide](https://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide#Z_steps):

    (200 * 16) / 1.25 = 2560

Nope. It's an `M16 threaded rod` according to [Metric Thread Pitch and
Tapping Sizes](http://www.metrication.com/engineering/threads.html).

    (200 * 16) / 2.0 = 1600

----

According to a [Marlin firmware User Guide for
Beginners](https://solidutopia.com/marlin-firmware-user-guide-basic/):

### Axis steps per unit

The stepper motor receives step by step moving command from the
controller. The controller needs to know the steps/mm ratio to send
the appropriate steps to reach the required distance. How many steps
are needed to move an axis by 1 mm?

#### Belts and pulley (usually xy axis):

        steps_per_mm = (motor_steps_per_rev * driver_microstep) / (belt_pitch * pulley_number_of_teeth)

#### lead screw (z axis)

        steps_per_mm = (motor_steps_per_rev * driver_microstep) / thread_pitch

#### Direct drive extruder:

        e_steps_per_mm = (motor_steps_per_rev * driver_microstep) / (hob_effective_diameter * pi)

The Lulzbots use a direct drive extruder, which is part of the
extruder / hot end assembly. The Rostock uses a Bowden extruder, which
affixed to the top of the Rostock, and is separated from the hot end
by a long tube.  See both at [How To Calibrate Your
Extruder](https://www.matterhackers.com/articles/how-to-calibrate-your-extruder).
See the example of the [Bowden
extruder](https://solidutopia.com/direct-drive-bowden-extruder/)

Extruder with gear reduction:

        e_steps_per_mm = (motor_steps_per_rev * driver_microstep) * (big_gear_teeth / small_gear_teeth) / (hob_effective_diameter * pi)

DEFAULT_AXIS_STEPS_PER_UNIT   {X,Y,Z,E1}

        #define DEFAULT_AXIS_STEPS_PER_UNIT   {80,80,200.0*8/3,760}

