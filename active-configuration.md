# Active code in Configuration.h

The stock Configuration.h file, sans comment lines and blank lines:

----

    #ifndef CONFIGURATION_H
    #define CONFIGURATION_H
    #define STRING_VERSION_CONFIG_H "2013-03-06" // build date and time
    #define STRING_CONFIG_H_AUTHOR "LulzBot" //Who made the changes.
    #define SERIAL_PORT 0
    #define BAUDRATE 115200
    #ifndef MOTHERBOARD
    #define MOTHERBOARD 301
    #endif
    #define POWER_SUPPLY 1
    #define TEMP_SENSOR_0 7
    #define TEMP_SENSOR_1 7
    #define TEMP_SENSOR_2 0
    #define TEMP_SENSOR_BED 1
    #define TEMP_RESIDENCY_TIME 10  // (seconds)
    #define TEMP_HYSTERESIS 3       // (degC) range of +/- temperatures considered "close" to the target one
    #define TEMP_WINDOW     1       // (degC) Window around target to start the recidency timer x degC early.
    #define HEATER_0_MINTEMP 5
    #define HEATER_1_MINTEMP 5
    #define HEATER_2_MINTEMP 5
    #define BED_MINTEMP 5
    #define HEATER_0_MAXTEMP 250
    #define HEATER_1_MAXTEMP 250
    #define HEATER_2_MAXTEMP 250
    #define BED_MAXTEMP 150
    #define PIDTEMP
    #define PID_MAX 256 // limits current to nozzle; 256=full current
    #ifdef PIDTEMP
      #define PID_FUNCTIONAL_RANGE 10 // If the temperature difference between
      #define PID_INTEGRAL_DRIVE_MAX 255  //limit for the integral term
      #define K1 0.95 //smoothing factor withing the PID
      #define PID_dT ((16.0 * 8.0)/(F_CPU / 64.0 / 256.0)) //sampling period of the
        #define  DEFAULT_Kp 22.2
        #define  DEFAULT_Ki 1.08
        #define  DEFAULT_Kd 114
    #endif // PIDTEMP
    #define MAX_BED_POWER 256 // limits duty cycle to bed; 256=full current
    #ifdef PIDTEMPBED
        #define  DEFAULT_bedKp 10.00
        #define  DEFAULT_bedKi .023
        #define  DEFAULT_bedKd 305.4
    #endif // PIDTEMPBED
    #define PREVENT_DANGEROUS_EXTRUDE
    #define PREVENT_LENGTHY_EXTRUDE
    #define EXTRUDE_MINTEMP 120
    #define EXTRUDE_MAXLENGTH (X_MAX_LENGTH+Y_MAX_LENGTH) //prevent extrusion of
    #define ENDSTOPPULLUPS // Comment this out (using // at the start of the line)
    #ifndef ENDSTOPPULLUPS
      #define ENDSTOPPULLUP_XMAX
      #define ENDSTOPPULLUP_YMAX
      #define ENDSTOPPULLUP_ZMAX
      #define ENDSTOPPULLUP_XMIN
      #define ENDSTOPPULLUP_YMIN
    #endif
    #ifdef ENDSTOPPULLUPS
      #define ENDSTOPPULLUP_XMAX
      #define ENDSTOPPULLUP_YMAX
      #define ENDSTOPPULLUP_ZMAX
      #define ENDSTOPPULLUP_XMIN
      #define ENDSTOPPULLUP_YMIN
      #define ENDSTOPPULLUP_ZMIN
    #endif
    const bool X_ENDSTOPS_INVERTING = true; // true = invert the logic of the endstops.
    const bool Y_ENDSTOPS_INVERTING = true; // true = invert the logic of the endstops.
    const bool Z_ENDSTOPS_INVERTING = true; // true = invert the logic of the endstops.
    #define X_ENABLE_ON 0
    #define Y_ENABLE_ON 0
    #define Z_ENABLE_ON 0
    #define E_ENABLE_ON 0 // For all extruders
    #define DISABLE_X false
    #define DISABLE_Y false
    #define DISABLE_Z false
    #define DISABLE_E false // For all extruders
    #define INVERT_X_DIR false   // for Mendel set to false, for Orca set to true
    #define INVERT_Y_DIR true    // for Mendel set to true, for Orca set to false
    #define INVERT_Z_DIR false   // for Mendel set to false, for Orca set to true
    #define INVERT_E0_DIR true   // for direct drive extruder v9 set to true, for geared extruder set to false
    #define INVERT_E1_DIR true   // for direct drive extruder v9 set to true, for geared extruder set to false
    #define INVERT_E2_DIR true   // for direct drive extruder v9 set to true, for geared extruder set to false
    #define X_HOME_DIR -1
    #define Y_HOME_DIR -1
    #define Z_HOME_DIR -1
    #define min_software_endstops true  //If true, axis won't move to coordinates
    #define max_software_endstops true  //If true, axis won't move to coordinates
    #define X_MAX_POS 298
    #define X_MIN_POS 0
    #define Y_MAX_POS 275
    #define Y_MIN_POS 0
    #define Z_MAX_POS 250
    #define Z_MIN_POS 0
    #define X_MAX_LENGTH (X_MAX_POS - X_MIN_POS)
    #define Y_MAX_LENGTH (Y_MAX_POS - Y_MIN_POS)
    #define Z_MAX_LENGTH (Z_MAX_POS - Z_MIN_POS)
    #define MANUAL_X_HOME_POS 0
    #define MANUAL_Y_HOME_POS 0
    #define MANUAL_Z_HOME_POS 0
    #define NUM_AXIS 4 // The axis order in all axis related arrays is X, Y, Z, E
    #define HOMING_FEEDRATE {50*60, 50*60, 4*60, 0}  // set the homing speeds (mm/min)
    #define DEFAULT_AXIS_STEPS_PER_UNIT   {100.5,100.5,800,800}  // default steps per unit for ultimaker
    #define DEFAULT_MAX_FEEDRATE          {500, 500, 10, 45}     // (mm/sec)
    #define DEFAULT_MAX_ACCELERATION      {9000,9000,100,10000}  // X, Y, Z, E maximum start speed for
    #define DEFAULT_ACCELERATION          500    // X, Y, Z and E max acceleration in mm/s^2 for printing moves
    #define DEFAULT_RETRACT_ACCELERATION  3000   // X, Y, Z and E max acceleration in mm/s^2 for r retracts
    #define DEFAULT_XYJERK                5.0    // (mm/sec)
    #define DEFAULT_ZJERK                 0.4    // (mm/sec)
    #define DEFAULT_EJERK                 5.0    // (mm/sec)
    #define EEPROM_SETTINGS
    #define EEPROM_CHITCHAT
    #if defined(ULTIMAKERCONTROLLER) || defined(REPRAP_DISCOUNT_SMART_CONTROLLER) || defined(G3D_PANEL)
     #define ULTIPANEL
     #define NEWPANEL
    #endif
    #define PLA_PREHEAT_HOTEND_TEMP 180
    #define PLA_PREHEAT_HPB_TEMP 70
    #define PLA_PREHEAT_FAN_SPEED 255      // Insert Value between 0 and 255
    #define ABS_PREHEAT_HOTEND_TEMP 240
    #define ABS_PREHEAT_HPB_TEMP 100
    #define ABS_PREHEAT_FAN_SPEED 255      // Insert Value between 0 and 255
    #ifdef ULTIPANEL
      #define SDSUPPORT
      #define ULTRA_LCD
      #define LCD_WIDTH 20
      #define LCD_HEIGHT 4
    #else //no panel but just lcd
      #ifdef ULTRA_LCD
        #define LCD_WIDTH 16
        #define LCD_HEIGHT 2
      #endif
    #endif
    #include "Configuration_adv.h"
    #include "thermistortables.h"
    #endif //__CONFIGURATION_H

----

